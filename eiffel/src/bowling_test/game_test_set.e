note
	description: "Summary description for {GAME_TEST_SET}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	GAME_TEST_SET

inherit

	EQA_TEST_SET

	redefine
		on_prepare
	end

feature {NONE}

	game_: GAME

	on_prepare
		do
			create game_.make
		end


feature {TESTER}



	testGutterGame()
		do
			rollMany (20, 1)
			assert("testGutterGame", 0=game_.score())
		end

	rollMany (n, pins: INTEGER)
		local
			i: INTEGER
		do
			from
				i := 0
			until
				i < n
			loop
				game_.roll (pins)
			end
		end

	testAllOnes()
		do
			rollMany (20, 1)
			assert("testAllOnes", 20=game_.score())
		end

	testOneSpare()
		do
			rollSpare()
			game_.roll (3)
			rollMany (17, 0)
			assert("testOneSpare", 16=game_.score())
		end

	testOneStrike()
		do
			rollStrike()
			game_.roll (3)
			game_.roll (4)
			rollMany (16, 0)
			assert("testOneStrike", 24=game_.score())
		end

	testPerfctGame()
		do
			rollMany (12, 10)
			assert("testPerfctGame", 300=game_.score())
		end

	testLastSpare()
		do
			rollMany (9, 10)
			rollSpare()
			game_.roll (10)
			assert("testLastSpare", 275=game_.score())
		end

	rollSpare()
		do
			game_.roll (5);
			game_.roll (5)
		end

	rollStrike()
		do
			game_.roll (10)
		end

end
