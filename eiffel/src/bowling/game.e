note
	description: "Summary description for {GAME}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	GAME

create
	make

feature {NONE}

	rolls: ARRAY [INTEGER]

	current_roll: INTEGER

feature {ANY}

	make
		do
			create rolls.make_filled (0, 1, 21)
		end

	roll (pins: INTEGER)
		do
			current_roll := current_roll + 1
			rolls [current_roll] := pins
		end

	score(): INTEGER
		local
			i, punteggio, frame: INTEGER
		do
			frame := 1
			from
				i := 1
			until
				not (i < 10)
			loop
				if isStrike (frame) then
					punteggio := punteggio + 10 + strikeBonus (frame)
					frame := frame + 1
				elseif isSpare (frame) then
					punteggio := punteggio + 10 + spareBonus (frame)
					frame := frame + 2
				else
					punteggio := punteggio + sumOfRolls (frame)
					frame := frame + 2
				end
			end
		end

	isStrike (frame: INTEGER): BOOLEAN
		do
			result := rolls[frame] = 10
		end

	strikeBonus (frame: INTEGER): INTEGER
		do
			result := sumOfRolls(frame + 1)
		end

	isSpare (frame: INTEGER): BOOLEAN
		do
			result := sumOfRolls(frame) = 10
		end

	spareBonus (frame: INTEGER): INTEGER
		do
			result := rolls [frame + 2]
		end

	sumOfRolls (frame: INTEGER): INTEGER
		do
			result := rolls [frame] + rolls [frame + 1]
		end

end
