note
    description: "Esempio base per iniziare"
    author: "Mattia Monga"

class
    HELLO

create
    make

feature -- sezione: puo` contenere molte feature
	make
		local
		  s: STRING
      n: INTEGER
		do
			   s := "Hello"
         n := 42
            print (s + " World! (con print)%N")
            io.put_string ("The answer is: " + n.out); io.put_new_line
		end
end
